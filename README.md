Dreadnought
===========

An automated aetherhunting system for Lusternia, written in Lua for the Mudlet client.

Installation
----------

1. Click on the `releases` button above and download the latest `Source Code (zip)` file for the latest Dreadnought release.
2. Extract the zip file somewhere on your computer (such as your Downloads folder).
3. Open Mudlet. On the Menu Bar, click on `Package Manager`.
4. Click on `Install`, navigate to the folder where you extracted the zip file, and select the `Dreadnought.xml` file.
5. Click `OK` to close the Package Manager window.
6. Restart Mudlet to make sure all variables are instantiated properly.

Updating
--------

1. Open up Mudlet's Package Manager.
2. Select the `Dreadnought` package and click `Uninstall`.
3. Restart Mudlet to clear out all existing variables.
4. See the above Installation instructions.

Documentation
-------------

Each of the four main aetherhunting roles has its own document detailing how to use the proper commands. You can find them within the [docs/](./docs/) directory within this repository.

License
-------

Dreadnought is dual-licensed under the Unlicense/MIT licenses.